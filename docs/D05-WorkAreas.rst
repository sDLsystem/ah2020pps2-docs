D05-WorkAreas
=============

------------

AH2020.PPS1
~~~~~~~~~~~

------------

.. [AH2020.PPS1] **Ergonomy, colaborative robotics, staffing according associate skills and physical conditions**

- **coordination**: [UA]_

- **shortDescription**: Proposes a solution based on a real-time active ergonomic management system with connectivity and dynamic interaction between the self-postural control monitoring device and modified safety boots of industrial workers to prevent fatigue and lesions from repetitive tasks. The system will collect information about posture, muscular tension of trunk and upper body, balance, and provide real-time feedback to give the opportunity of preventive actions. A compression and vibration device incorporated at the safety boots will stimulate the venous system for the prevention of fatigue symptoms. Focuses on development of solutions based on collaborative robots (cobots) aiming at the deployment of advanced human-machine interaction for task execution sharing. The coexistence of human operators and robots in shared spaces requires a distinct approach from the traditional notions of mutually exclusive volumes of operation. This is to be achieved by an intelligent real time monitoring of the full 3D workspace volume along with contact-free and contact-based interfaces between the human and the cobot and using strategies for collaborative skill sequencing.

------------

AH2020.PPS2
~~~~~~~~~~~

------------

.. [AH2020.PPS2] **Big data and predictive analytics for i4.0**

- **coordination**: [CMF]_

- **shortDescription**: Proposes to create interrelated and integrated solutions for problems, currently faced by the industry, related to predictive maintenance, data-driven asset/plant performance optimization, and data-driven quality control by taking advantage of mathematical modelling and recent machine learning techniques that have been proven to be effective in various sectors with impact on productivity and reduction of operational costs.

- **mainHumanResources**:

 - **Bosch Building Technology**:

  - **members_core**:

  .. [BBT.AL] António Leite [contactPoint]

 - **Bosch Thermo Technology**:

  - **members_core**:

  .. [BTT.DA] Duarte Almeida (AvP/MFD) [contactPoint]

  .. [BTT.DP] Diogo Pinho (AvP/MFD)

  .. [BTT.JN] Jorge Neves (AvP/MFD)

  .. [BTT.MJL] Maria João Lopes (TT/MFD-Av) -- PhD Business Innovation UA -- supervision [UA.ER]_ [UA.PG]_

  .. [BTT.NF] Nelson Ferreira (TT/MFD-Av) [projectManagement]

  .. [BTT.PR] Pedro Ramalho (AvP/MFD)

 - **Critical Manufacturing**:

  - **members_core**:

  .. [CMF.IS] Ivo Santos [contactPoint]

  .. [CMF.RM] Ricardo Magalhães

 - **Faculdade de Ciências da Universidade do Porto**:

  - **members_core**:

  .. [FCUP.ID] Inês Dutra [contactPoint]

  - **grants_active**:

  .. [FCUP.AM] Alexandre Macris -- 2022/05/16 - 2023/06/31 -- supervision [FCUP.ID]_

  - **grants_inactive**:

  .. [FCUP.FR] Filipe Rocha -- 2021/05/01 - 2021/12/31 -- supervision [FCUP.ID]_

 - **Oliveira & Irmão - Sistemas Sanitários**:

  - **members_core**:

  .. [OLI.RA] Ricardo Antunes [contactPoint]

 - **Universidade de Aveiro**:

  - **members_core**:

  .. [UA.AM] Ana Moura

  .. [UA.AV] António Valente [projectManagement]

  .. [UA.ER] Eugénio Rocha [contactPoint]

  .. [UA.GB] Gareth Baxter

  .. [UA.JPS] José Paulo Santos

  - **members_other**:

  .. [UA.AS] Amaro de Sousa

  .. [UA.DG] Diogo Gomes

  .. [UA.PG] Pétia Georgieva

  - **grants_active**:

  .. [UA.AB] Ângela Brochado (PhD EGI) -- 2021/01/01 - 2023/06/31 -- supervision [UA.CP]_ [UA.ER]_

  .. [UA.DC] Diogo Costa (PhD Mechanics) -- 2021/01/01 - 2023/06/31 -- supervision [UA.JPS]_ [UA.ER]_

  .. [UA.FB] Filipe Barroso (PhD Physics) -- 2021/01/01 - 2023/06/31 -- supervision [UA.GB]_ [UA.DG]_

  - **support_active**:

  .. [UA.BM] Bruno Mendes (MSc Mechanics) -- supervision [UA.JPS]_ [UA.ER]_ -- Internership at [OLI]_ supervided by [OLI.RA]_

  .. [UA.CP] Carina Pimentel (EGI)

  .. [UA.DM] Doaa Mohamed (MSc Mathematics) -- 2022/01/01 - 2022/06/31 -- supervision [UA.ER]_ [BTT.PR]_

  .. [UA.JM] Joana Martins (Computational Eng.) -- 2022/06/01 - 2022/08/01 -- supervision [UA.ER]_

  .. [UA.PN] Pedro Nunes (PhD Mechanics) -- 2021/01/01 - 2023/06/31 -- Bolsa FCT -- supervision [UA.JPS]_ [UA.ER]_

  .. [UA.PC] Pedro Cozinheiro (MSc Mechanics) -- 2021/09/01 - 2022/06/31 -- supervision [UA.JP]_ [UA.ER]_

  .. [UA.RP] Rui Pereira (MSc Mechanics) -- 2021/09/01 - 2022/06/31 -- Internership at [BTT]_ supervised by [BTT.DP]_ -- supervision [UA.JP]_ [UA.ER]_

  - **support_inactive**:

  .. [UA.DCL] Daniel Coelho (MSc Mechanics) -- 2021/02/01 - 2021/06/31 -- supervision [UA.JPS]_ [UA.ER]_ -- Thesis mark was 20 in a scale of 20.

  .. [UA.MP] Mariana Pinto (MSc Mathematics and Applications) -- supervision [UA.ER]_ -- Thesis mark was 19 in a scale of 20.

- **management**:

 - **deliverables**:

  .. [D11.1] Common Data Formats (CDF) version 1.0

  - **url**: `https://ah2020pps2-docs.readthedocs.io <https://ah2020pps2-docs.readthedocs.io/en/latest/AH2020.PPS2/files/D11.4.pdf>`_

  .. [D12.1] Specification of sensors and devices needed

  - **url**: `https://ah2020pps2-docs.readthedocs.io <https://ah2020pps2-docs.readthedocs.io/en/latest/AH2020.PPS2/files/D12.1.pdf>`_

------------

AH2020.PPS3
~~~~~~~~~~~

------------

.. [AH2020.PPS3] **IoT, 5G, conectivity**

- **coordination**: [BTT]_

- **shortDescription**: The purpose of PPS3 is to aggregate Industrial Internet of Things (IIoT) and 5G connectivity to bring the best technologies to the Industry in order to address typical challenges in the shop floor, improve Industrial processes (flexibility, efficiency, productivity, time sensitive communications, etc.) and build a base to new business models enabling circular economy promotion. Its main activities are the analysis of requirements and specifications; Requirements Analysis and specifications; IoT 5G Infrastructure Development; 5G Gateway Development; Component Development for 5G IoT end devices, 5G tag, and Smart Wearables; Energy Management Solution for Industrial IoT; Hardware and Software Integration and Prototypes validation.

------------

AH2020.PPS4
~~~~~~~~~~~

------------

.. [AH2020.PPS4] **Virtual and augmented reality applied to the Industry**

- **coordination**: [BBT]_

- **shortDescription**: It aims to use computer vision technologies to increase flexibility in industrial operations, optimizing control points, freeing operators from tedious tasks and providing advanced and flexible user interfaces for interacting with IT systems.

------------

AH2020.PPS5
~~~~~~~~~~~

------------

.. [AH2020.PPS5] **Human resources and attraction of new talents for the Industry**

- **coordination**: [UA]_

- **shortDescription**: PPS5 is meant to optimize human efficiency and productivity, for better and healthier manufacturing. This PPS demonstrates how Industry 4.0 technologies (digital technologies and big data) can be used to upgrade standard management practices at the level of human resources, by enabling the gathering and integration of non-structured information in a structured management decision framework. We propose to develop an integrated technological solution for integrating and analysing intertwined aspects of human resources (HR).

------------

AH2020.PPS6
~~~~~~~~~~~

------------

.. [AH2020.PPS6] **Management, dissemination and demonstrator**

- **coordination**: [BTT]_ [UA]_

- **shortDescription**: PPS6 deals with the management tasks of the project.


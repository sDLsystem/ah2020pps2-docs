D07-WorkPackages
================

------------

AH2020.PPS2.M
~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.M] **Predictive and Conditional-based Maintenance**

- **D09-CriticalSuccessFactors**: For each stop type code, the number of stops needs to be enough to have useful information for the algorithms, otherwise there is no way to "learn/undertand" to stop "pattern/reason".

- **D10-Partners**: [BBT]_ [BTT]_ [CMF]_ [FCUP]_ [OLI]_ [UA]_

- **D11-ENESII**: [FCUP]_ [UA]_

- **D12-EstimatedRisks**: The number of time blocks with the (unplanned) stop mode may be quite low compared with the number of time blocks of normal working mode, which makes the dataset highly inbalance and do not allow to attain reasonable ML metric results; Sensor data retrieved from existent or added hardware is not enough to have relevant features for the ML algorithms and/or the degradation model algorithms.

- **D13-challengesScientific**: In maintenance, UseCases have specific features that are difficult to generalize to be dealt by a big algorithm; Too many different approaches in the literature; Inbalance classes; Labelling generation issues; Sensor noise.

- **D14-challengesCommercial**: UseCases have specific features turn deployment phase more complex.

- **D15-InnovativeCharacteristics**: [IC01]_ [IC02]_ [IC05]_ [IC06]_

- **D17-Frameworks**: [FAMTS]_ [FEFF]_

------------

AH2020.PPS2.B
~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.B] **Bottleneck analysis**

- **D09-CriticalSuccessFactors**: Enough data with "Minimal Information".

- **D10-Partners**: [BBT]_ [BTT]_ [CMF]_ [FCUP]_ [OLI]_ [UA]_

- **D11-ENESII**: [FCUP]_ [UA]_

- **D12-EstimatedRisks**: The manufacturing lines may be to much balanced, implying that the bottleneck machine shifts in time to often, which do not allow to find reasonable correction measures.

- **D13-challengesScientific**: Too many different theoretical concepts of the notion of bottleneck in the literature, which do not directly applied to the problem under study.

- **D14-challengesCommercial**: Reasonable challenges.

- **D15-InnovativeCharacteristics**: [IC01]_ [IC07]_

- **D17-Frameworks**: [FBIP]_

------------

AH2020.PPS2.Q
~~~~~~~~~~~~~

------------

.. [AH2020.PPS2.Q] **Quality analysis**

- **D08-Smart**: TBD

- **D09-CriticalSuccessFactors**: TBD

- **D10-Partners**: [BBT]_ [BTT]_ [CMF]_ [FCUP]_ [OLI]_ [UA]_

- **D11-ENESII**: [FCUP]_ [UA]_

- **D12-EstimatedRisks**: TBD

- **D13-challengesScientific**: TBD

- **D14-challengesCommercial**: TBD

- **D15-InnovativeCharacteristics**: [IC03]_ [IC04]_ [IC08]_

- **D17-Frameworks**: [FNPRC]_ [FPRVF]_


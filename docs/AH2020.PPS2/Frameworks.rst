AH2020.PPS2 Frameworks
======================

.. [FAMTS] **Frameworks for Adaptive Maintenance Time Scheduling**

- Project Frameworks:

 - **FAMTS_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FAMTS_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **FAMTS_bDinG-BTT01**:

  - **Status Phase**: Draft

  - **Common Data Format**: :ref:`FAMTS_bDinG-BTT01`

  - **Description**:

   Read the [BTT]_ file format for **Smart Presses** and inject it to the internal analytics database.
   
   
 - **FAMTS_bDinG-OLI01**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FAMTS_bDinG-OLI01`

  - **Description**:

   Read the [OLI]_ file format for **Injection Mold Machine** and inject it to the internal analytics database.
   
   
 - **FAMTS_edaG**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FAMTS_edaG`

  - **Description**:

   Exploratory data analysis reporting.
   
   
 - **FAMTS_fGeneralizedFaultTreesG**:

  - **Status Phase**: Under adjusments to accomodate OLI UseCase [UC04B]_

  - **Common Data Format**: :ref:`FAMTS_fGeneralizedFaultTreesG`

  - **Description**:

   From sensors data, this framework finds the best Generelized Fault Trees (GFTs) structures that models several classes of machine failures (see [P08]_ [P07]_).
   
   
 - **FAMTS_maGeneralizedFaultTreesG**:

  - **Status Phase**: Under adjusments to accomodate OLI UseCase [UC04B]_

  - **Common Data Format**: :ref:`FAMTS_maGeneralizedFaultTreesG`

  - **Description**:

   In real-time and using Generelized Fault Trees (GFTs) structures, this framework makes predictions about failure events of components in a machine that may be repaired or replaced, answering questions as:
    
   - What is the expected time of failure (e.g. break) of the current component?
    
   - What is the probability of failure of the current component?
   
   
.. [FBIP] **Frameworks for Bottleneck Identification and Prediction**

- Project Frameworks:

 - **FBIP_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FBIP_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **FBIP_bDinG-BTT01**:

  - **Status Phase**: Will be soon deployed for validation

  - **Common Data Format**: :ref:`FBIP_bDinG-BTT01`

  - **Description**:

   Read the [BTT] file format and inject it to the internal analytics database.
   
   
 - **FBIP_fBottleneckIdentificationG**:

  - **Status Phase**: Will be soon deployed for validation

  - **Common Data Format**: :ref:`FBIP_fBottleneckIdentificationG`

  - **Description**:

   Calculate several bottleneck metrics. This framework has the following capabilities [level>=A]:
    
   - CA01: Automatic determination of the locationId sequences;
    
   - CA02: Calculation of the transition probability matrix from location i into j;
    
   - CA03: Identification of reprocessed partnumberID at the same locationID;
    
   - CA04: Identification of partnumberID belonging to the previous shift;
    
   - CA05: Identification of partnumberID not concluded in the current shift;
    
    and the capabilities [level>=B]:
    
   - CA06: Determination of the Ideal Cycle Time (ICT) for each locationID;
    
   - CA07: Calculation of several bottleneck metrics as sole bottleneck, shifting bottleneck, queue bottleneck, beside others.
   
   
 - **FBIP_fBottleneckPredictionG**:

  - **Status Phase**: Will be soon deployed for validation

  - **Common Data Format**: :ref:`FBIP_fBottleneckPredictionG`

  - **Description**:

   Train several machine learning classifiers to determine the best model that forecast the bottleneck machine in the next N minutes. See details in [P04]_.
   
   
 - **FBIP_maBottleneckPredictionG**:

  - **Status Phase**: Will be soon deployed for validation

  - **Common Data Format**: :ref:`FBIP_maBottleneckPredictionG`

  - **Description**:

   Deploy the best model, trained with :ref:`FBIP_fBottleneckPredictionG`, to forecast the bottleneck machine in the next N minutes.
   
   
.. [FEFF] **Frameworks for Equipment Failure Forecasting**

- Project Frameworks:

 - **FEFF_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FEFF_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **FEFF_bDinG-BTT01**:

  - **Status Phase**: Draft

  - **Common Data Format**: :ref:`FEFF_bDinG-BTT01`

  - **Description**:

   Read the [BTT]_ file format for **Smart Presses** and inject it to the internal analytics database.
   
   
 - **FEFF_bDinG-OLI01**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FEFF_bDinG-OLI01`

  - **Description**:

   Read the [OLI]_ file format for **Injection Mold Machine** and inject it to the internal analytics database.
   
   
 - **FEFF_edaG**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FEFF_edaG`

  - **Description**:

   Exploratory data analysis reporting.
   
   
 - **FEFF_fFailurePredictionG**:

  - **Status Phase**: Under adjusments to accomodate OLI UseCase [UC04A]_.

  - **Common Data Format**: :ref:`FEFF_fFailurePredictionG`

  - **Description**:

   From sensors data and event data, this framework do feature engineering, dimension reduction, anomaly detection, and trains several machine learning classifiers. See the details in [P05]_.
   
   
 - **FEFF_maFailurePredictionG**:

  - **Status Phase**: Under adjusments to accomodate OLI UseCase [UC04A]_

  - **Common Data Format**: :ref:`FEFF_maFailurePredictionG`

  - **Description**:

   For each failure event class, this framework deploys the best model in order to forecast failures in the next T minutes.
   
   
.. [FNPRC] **Frameworks for NOK Prioritization and Root Cause**

- Project Frameworks:

 - **FNPRC_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FNPRC_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **FNPRC_bDinG-BBT01**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FNPRC_bDinG-BBT01`

  - **Description**:

   Read the [BTT]_ csv files and inject then to the internal analytics database.
   
   
 - **FNPRC_fStepsAnalysisG**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FNPRC_fStepsAnalysisG`

  - **Description**:

   Produce statistics about the steps modes OK vs NOK in quality tests, namelly, finding the most frequent steps that fail and the pairs of steps the fail simultaneously, beside other metrics.
   
   
 - **FNPRC_fStepsPredictionG**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FNPRC_fStepsPredictionG`

  - **Description**:

   From step information, this framework do feature enginerring and trains several machine learning classifiers to find a good model for step NOK forecast.
   
   
 - **FNPRC_maStepsPredictionG**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FNPRC_maStepsPredictionG`

  - **Description**:

   Uses the (best) model trained in :ref:`FNPRC_fStepsPredictionG`.
   
   
- Supplementary Frameworks:

 - **FNPRC_bDinG-BTT01**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FNPRC_bDinG-BTT01`

  - **Description**:

   Read the [BTT]_ file format and inject it to the internal analytics database.
   
   
 - **FNPRC_fStepsOptimalOrderG**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FNPRC_fStepsOptimalOrderG`

  - **Description**:

   Analysis the order of the steps in a set of test and proposes a new order that minimized the global time of testing.
   
   
.. [FPRVF] **Frameworks for Partnumber Rejection Validation and Forecasting**

- Project Frameworks:

 - **FPRVF_sys**:

  - **Status Phase**: In development phase

  - **Common Data Format**: :ref:`FPRVF_sys`

  - **Description**:

   Framework for Product-UseCase services management
   
   
 - **FPRVF_bDinG-BTT01**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FPRVF_bDinG-BTT01`

  - **Description**:

   Read the [BTT]_ file format and inject it to the internal analytics database.
   
   
 - **FPRVF_fRejectionPredictionG**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FPRVF_fRejectionPredictionG`

  - **Description**:

   From sensors data and event data, this framework do feature engineering, dimension reduction, anomaly detection, and trains several machine learning classifiers. See the details in [P09]_.
   
   
 - **FPRVF_maRejectionPredictionG**:

  - **Status Phase**: Planning deploymemt phase for validation

  - **Common Data Format**: :ref:`FPRVF_maRejectionPredictionG`

  - **Description**:

   Use the (best) model trained in :ref:`FPRVF_fRejectionPredictionG`.
   
   

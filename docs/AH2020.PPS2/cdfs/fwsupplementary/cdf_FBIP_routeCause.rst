.. _FBIP_routeCause:

FBIP_routeCause (cdf)
==================

:version: 0.9
:framework class: :ref:`fbip`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: E. Rocha (UA)  
:conception team: E. Rocha (UA)


1. Description
~~~~~~~~~~~~~~

Determine the contributions of process features  for a machine to be a bottleneck.

2. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database, in particular, from the outputs of the framework :ref:`FBIP_identify`.

3. Output Data
~~~~~~~~~~~~~~

Results are kept in the internal analytics database, shared in PDF reports, and available for other PPS modules, e.g. for use in augmented reality.

4. Components
~~~~~~~~~~~~~

-----------

:Identification: RC01
:Description: Determine the contributions of process features  for a machine to be a bottleneck
:Implementation team: E. Rocha (UA)
:Plugins: This component uses the followin plugins

- :ref:`plugin_rc01_processRootCause`

------------

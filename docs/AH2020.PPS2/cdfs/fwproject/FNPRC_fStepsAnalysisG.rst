.. _FNPRC_fStepsAnalysisG:

FNPRC_fStepsAnalysisG (cdf)
===========================

:version: 0.9
:framework class: :ref:`fnprc`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.ER]_ [BBT.AL]_ 

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database, inject from :ref:`FNPRC_bDinG-BBT01`.

2. Output Data
~~~~~~~~~~~~~~

Results are kept in the internal analytics database.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fStepsAnalysisG_RUN01
:Description: Produce step statistics in quality tests
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_x07_stepStats`

------------

.. _FPRVF_bDinG-BTT01:

FPRVF_bDinG-BTT01 (cdf)
=======================

:version: 0.9
:framework class: :ref:`fprvf`
:input data format: batch / csv file
:data injection technique: cron file watch
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: --
:conception team: [BTT.DA]_ [BTT.PR]_ [UA.ER]_


1. Input Data
~~~~~~~~~~~~~

=================  =======================  ====================  ====================  =================
Source              Field                    Type                  Data Level            Observations
=================  =======================  ====================  ====================  =================
Sensor Data         dt                       datetime               A                     [1]
Sensor Data         locationID               varchar(150)           A
Sensor Data         param_name               varchar(255)           A                     [2]
Sensor Data         result_value             float                  A                     [3]
Sensor Data         unit                     varchar(16)            A                     [4]
Sensor Data         lower_tolerance          float                  A                     [5]
Sensor Data         upper_tolerance          float                  A                     [6]
Sensor Data         result_state             float                  A                     [7]   
Sensor Data         workcycle_counter        float                  A                     [8]    
=================  =======================  ====================  ====================  =================

Table Observations:

- [1]: Timestamp obtained when the measure is taken.

- [2]: Name of the Sensor

- [3]: Value Measured by the sensor

- [4]: Unit of the value that was measured

- [5]: Lower limit for the value measured

- [6]: Upper limit for the value measured

- [7]: Evaluation of the process of execution

- [8]: Indicates the number of reworks or if a part was processed multiple times

2. Output Data
~~~~~~~~~~~~~~

Inject into [FPRVF_DinSDL]_.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/bDinG.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: bDinG-BTT01_INJ01
:Description: Watch a directory for a new csv file and inject it to the internal analytics database in a normalized format.
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

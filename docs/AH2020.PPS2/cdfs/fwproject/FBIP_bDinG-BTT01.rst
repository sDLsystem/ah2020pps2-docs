.. _FBIP_bDinG-BTT01:

FBIP_bDinG-BTT01 (cdf)
======================

:version: 0.9
:framework class: :ref:`fbip`
:input data format: batch / csv file
:data injection technique: cron file watch
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: --
:conception team: [BTT.DA]_ [BTT.PR]_ [UA.ER]_


1. Input Data
~~~~~~~~~~~~~

===============  =======================  ====================  ====================
Field            Type                     Data Level            Observations
===============  =======================  ====================  ====================
dt               datetime                 A                     [1]
locationID       varchar(100)             A
partnumberID     varchar(150)             A
processingTime   float                    B                     [2]
processingTags   string                   B                     [3]
===============  =======================  ====================  ====================

Observations:

- [1] Timestamp obtained when the partnumID leaves the locationID;

- [2] Time in seconds representing the processing time of the partnumberID on the locationID;

- [3] Space separated string with tags labelling the processingTime (e.g. stop_maintenance_curative)

2. Output Data
~~~~~~~~~~~~~~

Inject into table [FBIP_DinSDL]_.

3. Components
~~~~~~~~~~~~~

------------

:Identification: bDinG-BTT01_INJ01
:Description: Inject dataset D01 (L7 / day 2020-11-23 / shift T01)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ02
:Description: Inject dataset D02 (L7 / day 2021-02-08 / shift T01)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ03
:Description: Inject dataset D03 (L7 / day 2021-02-08 / shift T02)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ04
:Description: Inject dataset D04 (L7 / day 2021-02-09 / shift T01)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ05
:Description: Inject dataset D05 (L7 / day 2021-02-09 / shift T02)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ06
:Description: Inject dataset D06 (L7 / day 2021-02-17 / shift T01)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

-----------

:Identification: bDinG-BTT01_INJ07
:Description: Inject dataset D07 (L7 / day 2021-02-17 / shift T02)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

:Identification: bDinG-BTT01_INJ08
:Description: Inject dataset D06 (L7 / day 2021-02-18 / shift T01)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

-----------

:Identification: FBIP_bDinG-BTT01_INJ09
:Description: Inject dataset D07 (L7 / day 2021-02-18 / shift T02)
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins.

------------

.. _FPRVF_maRejectionPredictionG:

FPRVF_maRejectionPredictionG (cdf)
==================================

:version: 0.9
:framework class: :ref:`fprvf`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.ER]_ [UA.DC]_ [UA.PR]_

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the outputs of :ref:`FPRVF_fRejectionPredictionG`.

2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database and shared with other systems.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/maNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fRejectionPredictionG_PRED01
:Description: (Information will be added, when available)
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_tb03_generalRegressor_predict`

------------

:Identification: fRejectionPredictionG_PRED02
:Description: (Information will be added, when available)
:Implementation team: [UA.DC]_ [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_x10_Rejection_predict`

------------

.. _FBIP_sys:

FBIP_sys (cdf)
===============

:version: 1.0
:framework class: :ref:`fbip`
:input data format: ---
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: ---
:conception team: [UA.ER]_

1. Input Data
~~~~~~~~~~~~~

Configuration files.

2. Output Data
~~~~~~~~~~~~~~

Manage the generation of all SQL tables and documentation outputs.

.. [FBIP_DinSDL] The DinSDL table for the [FBIP]_ Product-UseCase

===============  =======================  ====================  ====================
Field            Type                     Data Level            Observations
===============  =======================  ====================  ====================
dt               datetime                 A                     [1]
dtint            bigint                   A                     [1]
locationID       varchar(100)             A                     [2]
partnumberID     varchar(150)             A
processingTime   float                    B                     [3]
processingTags   string                   B                     [4]
===============  =======================  ====================  ====================

Table Observations:

- [1]: Datetime string with the format "yyyy/mm/dd HH:MM:SS.f" and its corresponding UTC integer; Timestamp obtained when the partnumID leaves the locationID

- [2]: The location identification of the UseCase

- [3]: Time in seconds representing the processing time of the partnumberID on the locationID;

- [4]: Space separated string with tags labelling the processingTime (e.g. stop_maintenance_curative)

General Observations:

- The data corresponds to the Minimal Information defined in [P01]_ and [P02]_.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/sys.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: sys_install
:Description: (Re)create default system tables 
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_config
:Description: Set system configurations 
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_docs
:Description: Generate Product-UseCase documentation  
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

:Identification: sys_main
:Description: Execution of Product-UseCase maintenance tasks and management of services  
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

------------

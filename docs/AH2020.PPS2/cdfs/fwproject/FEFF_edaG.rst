.. _FEFF_edaG:

FEFF_edaG (cdf)
==================

:version: 0.9
:framework class: :ref:`feff`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: html reports
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.DC]_ [UA.ER]_

1. Input Data
~~~~~~~~~~~~~

For the current data in the table [FEFF_DinSDL]_ and for each locationID, proceed with an exploratory data analysis (EDA) with the corresponding generation of a html report.

2. Output Data
~~~~~~~~~~~~~~

The EDA html report.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/edaG.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: FEFF_edaG_main
:Description: Exploratory Data Analysis for each locationID
:Implementation team: [UA.DC]_ [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_eda`

------------

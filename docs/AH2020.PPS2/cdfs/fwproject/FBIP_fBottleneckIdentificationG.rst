.. _FBIP_fBottleneckIdentificationG:

FBIP_fBottleneckIdentificationG (cdf)
=====================================

:version: 0.9
:framework class: :ref:`fbip`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_ [UA.AM]_ [UA.AS]_ [BTT.DA]_ [UA.AB]_
:conception team: [UA.ER]_


1. Input Data
~~~~~~~~~~~~~

Data source is the table [FBIP_DinSDL]_ and some configuration files.

2. Output Data
~~~~~~~~~~~~~~

Results are kept in the internal analytics database, shared as PDF reports, and available for other PPS modules, e.g. for use in augmented reality (see [AH2020.PP4]_).

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

-----------

:Identification: fBottleneckIdentificationG_DASH01
:Description: Generate data for dashboardings visualization
:Implementation team: [UA.ER]_
:Plugins: Do not use plugins

-----------

:Identification: fBottleneckIdentificationG_FBIP01
:Description: Extract unique locationID, extract unique partnumberID, and determine the predominant localtionOrder and fix internal IDs for locations and partnumbers
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip01_path_order`

-----------

:Identification: fBottleneckIdentificationG_FBIP02
:Description: Validate paths and find part numbers not valid in this scenario and remove marked partnumberID
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip02_path_validation`

-----------

:Identification: fBottleneckIdentificationG_FBIP03
:Description: Calculate node bottleneck metrics (as AMPM, AQPM, and ATPM) and show some statistics
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip03_locstats`

-----------

:Identification: fBottleneckIdentificationG_FBIP04
:Description: Generate records for the start of the processingTime
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip04_end2start`

-----------

:Identification: fBottleneckIdentificationG_FBIP05
:Description: Recalculate processingTime metrics
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip05_stats`

-----------

:Identification: fBottleneckIdentificationG_FBIP06
:Description: Generate the state variables of the system
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip06_state`

-----------

:Identification: fBottleneckIdentificationG_FBIP07
:Description: Realculate node bottleneck metrics (AAPM variants as AMPM, AQPM, ATPM or median variants) and show some statistics
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip07_nodeBottleneck`

-----------

:Identification: fBottleneckIdentificationG_FBIP08
:Description: Propagate metrics from nodeID to data
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip08_node2data`

-----------

:Identification: fBottleneckIdentificationG_FBIP09
:Description: Generate the bottleneck metric graphs
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip09_graphs`

-----------

:Identification: fBottleneckIdentificationG_FBIP10
:Description: Build FBIP reports
:Implementation team: [UA.ER]_
:Plugins: The component uses the following plugins

- :ref:`plugin_fbip10_report`

------------

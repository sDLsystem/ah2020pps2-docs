.. _FAMTS_bDinG-BTT01:

FAMTS_bDinG-BTT01 (cdf)
======================

:version: 0.9
:framework class: :ref:`famts`
:input data format: batch / csv file
:data injection technique: cron file watch
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: --
:conception team: [BTT.DA]_ [BTT.PR]_ [UA.ER]_ [UA.DC]_ [UA.PN]_

1. Input Data
~~~~~~~~~~~~~~

.. [FAMTS_DinINST-BTT] Table for [BTT]_ smart presses data

=================  =======================  ====================  ====================  =================
 Source             Field                    Type                  Data Level            Observations
=================  =======================  ====================  ====================  =================
Sensor Data         dt                       datetime               A                     [1]
Sensor Data         sensorID                 varchar(150)           A
Sensor Data         param_name               varchar(255)           A                     [2]
Sensor Data         result_value             float                  A                     [3]
Sensor Data         unit                     varchar(16)            A                     [4]
Sensor Data         lower_tolerance          float                  A                     [5]
Sensor Data         upper_tolerance          float                  A                     [6]
Sensor Data         result_state             float                  A                     [7]   
Sensor Data         workcycle_counter        float                  A                     [8]    
Prod.Man.Data       locationID               varchar(150)           A                     [9]        
Prod.Man.Data       category                 varchar                A                     [10]
Prod.Man.Data       cause_number             int                    A                     [11]    
Prod.Man.Data       description              nvarchar2(2000)        A                     [12]    
Prod.Man.Data       i_class                  int                    A                     [13]
Prod.Man.Data       i_start                  datetime               A                     [14]    
Prod.Man.Data       i_end                    datetime               A                     [15]
Prod.Man.Data       duration                 datetime               A                     [16]    
Main. Data          order_type               varchar                A                     [17]    
Main. Data          order_number             varchar                A                     [18]    
=================  =======================  ====================  ====================  =================

Table Observations:

- [1]: Timestamp obtained when the measure is taken.
- [2]: Name of the Sensor
- [3]: Value Measured by the sensor
- [4]: Unit of the value that was measured
- [5]: Lower limit for the value measured
- [6]: Upper limit for the value measured
- [7]: Evaluation of the process of execution
- [8]: Indicates the number of reworks or if a part was processed multiple times
- [9]: Location where has ocurred the interruption
- [10] Interruption category
- [11]: Cause number of the interruption
- [12]: Interruption description
- [13]: (0 or 1) Indicates if an interruption was planed(0) or unplanned(1)
- [14]: Starting date and time of the interruption
- [15]: Ending date and time of the interruption
- [16]: Interruption duration
- [17]: Interruption type (PM01,PM02,PM03) 
- [18]: Number of the order launched to resolve the interruption 

General Observations:

- The mapping between column names (DinINST<->DinSDL) is defined in prjVG key D28-INSTSDL-Variables.

LocationIDs:

- smartpress01

2. Output Data
~~~~~~~~~~~~~~

Inject into table [FAMTS_DinSDL]_.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/bDinG.rst


4. Components
~~~~~~~~~~~~~

------------

:Identification: bDinG-BTT01_main
:Description: Watch a directory for a new csv file and inject it to the internal analytics database in a normalized format.
:Implementation team: [UA.PN]_ [UA.ER]_
:Plugins: Do not use plugins.

------------

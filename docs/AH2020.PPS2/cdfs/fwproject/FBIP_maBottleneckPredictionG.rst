.. _FBIP_maBottleneckPredictionG:

FBIP_maBottleneckPredictionG (cdf)
==================================

:version: 0.9
:framework class: :ref:`fbip`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_ [UA.PG]_ [BTT.MJL]_  
:conception team: [UA.ER]_ [BTT.MJL]_


1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database, inject by :ref:`FBIP_fBottleneckPredictionG`.

2. Output Data
~~~~~~~~~~~~~~

Results are kept in the internal analytics database, shared in PDF reports, and available for other PPS modules, e.g. for use in augmented reality (see [AH2020.PPS4]_).

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/maNNF.rst

4. Components
~~~~~~~~~~~~~

-----------

:Identification: maBottleneckPredictionG_PRED01
:Description: Forecast the bottleneck machine in the next N minutes
:Implementation team: [UA.ER]_
:Plugins: This component uses the following plugins

- :ref:`plugin_tb02_generalClassifier_predict`

------------

.. _FPRVF_fRejectionPredictionG:

FPRVF_fRejectionPredictionG (cdf)
=================================

:version: 0.9
:framework class: :ref:`fprvf`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_
:conception team: [UA.DC]_ [UA.ER]_

1. Input Data
~~~~~~~~~~~~~~

Input data is obtained from [FPRVF_DinSDL]_.

2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database to be used by the framework :ref:`FPRVF_maRejectionPredictionG`.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

------------

:Identification: fRejectionPredictionG_TRAIN01
:Description: (Information will be added, when available)
:Implementation team: [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_tb04_generalRegressor_predict`

------------

:Identification: fRejectionPredictionG_TRAIN02
:Description: (Information will be added, when available)
:Implementation team: [UA.DC]_ [UA.ER]_
:Plugins: The following are the plugins used

- :ref:`plugin_x10_Rejection_train`

------------

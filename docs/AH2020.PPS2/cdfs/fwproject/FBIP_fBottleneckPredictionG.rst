.. _FBIP_fBottleneckPredictionG:

FBIP_fBottleneckPredictionG (cdf)
=================================

:version: 0.9
:framework class: :ref:`fbip`
:input data format: internal analytics database
:data processing: internal analytics database
:data output: internal analytics database
:deploy format: microservice
:scientific team: [UA.ER]_ [UA.PG]_ [BTT.MJL]_  
:conception team: [UA.ER]_ [BTT.MJL]_

1. Input Data
~~~~~~~~~~~~~

Input data is obtained from the internal analytics database, from the outputs of the framework :ref:`FBIP_fBottleneckIdentificationG`.

2. Output Data
~~~~~~~~~~~~~~

Output data is kept in the internal analytics database for future use by :ref:`FAMTS_maBottleneckPredictionG`.

3. sbroETL information
~~~~~~~~~~~~~~~~~~~~~~

.. include:: ../../../ftemplates/fNNF.rst

4. Components
~~~~~~~~~~~~~

-----------

:Identification: fBottleneckPredictionG_TRAIN01
:Description: Train a Random Forest model to forecast of the next bottleneck
:Implementation team: [BTT.MJL]_
:Plugins: This component uses the following plugins

- :ref:`plugin_x03_MJ_FBIPpredict`

-----------

:Identification: fBottleneckPredictionG_TRAIN02
:Description: Train several classifiers for the forecast of the next bottleneck
:Implementation team: [UA.ER]_
:Plugins: This component uses the following plugins

- :ref:`plugin_tb01_generalClassifier_train`

------------

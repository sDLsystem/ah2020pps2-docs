Project Organization Diagram
============================

This project was developed over the sBRO Analytics Platform (sbroPA) and follows the DMAIC-PDCA diagram - version 2.2, which represents relevant metadata, data flows, implementation layers, validation steps, and deploy protocols. The information shown in this documentation follows this diagram structure/protocol.

The image and its information is owned by Smart Business and Research Observatory and may not be used without explicit authorization of the owner. **The copyright infringement will be punished by legal means**. 


.. figure:: images/DMAIC-PDCA.png
   :alt: DMAIC-PDCA diagram

.. figure:: images/sbroAP01.png
   :alt: sbroAP IN/OUT diagram

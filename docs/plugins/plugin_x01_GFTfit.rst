.. _plugin_x01_GFTfit:

x01_GFTfit
==========

:Version: 1.0
:Type: Plugin
:Developer: sBRO
:Aims: From data modelling a real physical phenomena (e.g., the failure of a component on a machine), this plugin finds the Generelized h-Fault Trees (GFTs) structure that best fit a target event distribution from N given basic events distributions.

.. _plugin_fbip02_path_validation:

fbip02_path_validation
======================

:Version: 1.0
:Tupe: Plugin
:Developer: [UA.ER]_
:Aims: Validate paths and find part numbers not valid in this scenario and remove marked partnumberID

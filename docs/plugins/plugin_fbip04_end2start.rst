.. _plugin_fbip04_end2start:

fbip04_end2start
================
 
:Version: 1.0
:Type: Plugin
:Developer: [UA.ER]_
:Aims: Generate records for the start of the processingTime

.. _plugin_x06_DC_FEFFpredict:

x06_DC_FEFFpredict
==================
 
:Version: 1.0
:Type: Plugin
:Developer: D. Costa (AH2020 PhD fellow / UA)
:Aims: Forecast failures of machines using component TRAIN01 of :ref:`plugin_x05_DC_FEFFtrain`

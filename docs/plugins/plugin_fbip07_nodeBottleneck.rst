.. _plugin_fbip07_nodeBottleneck:

fbip07_nodeBottleneck
=====================
 
:Version: 1.0
:Type: Plugin
:Developer: [UA.ER]_
:Aims: Realculate node bottleneck metrics (AAPM variants as AMPM, AQPM, ATPM or median variants) and show some statistics

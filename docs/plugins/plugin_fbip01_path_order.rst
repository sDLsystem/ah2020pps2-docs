.. _plugin_fbip01_path_order:

fbip01_path_order
=================
 
:Version: 1.0
:Type: Plugin
:Developer: [UA.ER]_
:Aims: Extract unique locationID, extract unique partnumberID, and determine the predominant localtionOrder and fix internal IDs for locations and partnumbers

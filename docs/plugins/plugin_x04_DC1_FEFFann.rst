.. _plugin_x04_DC1_FEFFann:

x04_DC1_FEFFann
==================
 
:Version: 1.0
:Type: Plugin
:Developer: D. Coelho (MSc student, UA), E. Rocha (UA)
:Aims: Find the last (reduced) windowed anomalies by COPOD
